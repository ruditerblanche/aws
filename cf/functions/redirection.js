function handler(event) {

    var request = event.request;
    var host = request.headers.host.value;

    var firstdomainresponse = {
        headers: {
            "location": {
                "value": `https://www.firstdomain.local${request.uri}`
            },
            "strict-transport-security": {
                "value": "max-age=63072000; includeSubDomains; preload"
            }
        },
        statusCode: 301,
        statusDescription: "Moved Permanently"
    };

    var seconddomainresponse = {
        headers: {
            "location": {
                "value": `https://www.seconddomain.local${request.uri}`
            },
            "strict-transport-security": {
                "value": "max-age=63072000; includeSubDomains; preload"
            }
        },
        statusCode: 301,
        statusDescription: "Moved Permanently"
    };

    if (host === "firstdomain.local") {
        return firstdomainresponse;
    }

    if (host === "seconddomain.local") {
        return seconddomainresponse;
    }

    return request;
}
